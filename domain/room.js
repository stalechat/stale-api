
const R         = require('ramda')
const Domain    = require('../lib/helper/domain.js')

const TABLE     = 'room'


const getById = Domain.getById(TABLE)

const deleteById = Domain.deleteById(TABLE)

const getByNameAndPassword = R.curry((db, name, password) => db.query(`
  SELECT * FROM ${TABLE}
  WHERE name = '${name}'
  AND ${wherePassword(password)}
  AND deleted = 0
`))


const getOrCreateRoom = R.curry((db, name, password) =>
  getByNameAndPassword(db, name, password)
  .then(rows => {
    if (R.isEmpty(rows)) return insert(db, name, password)
    return rows[0]
  })
)


const insert = R.curry((db, name, password) =>
  db.query(`
    INSERT INTO ${TABLE}
    (name, password)
    VALUES(${db.escape(name)}, ${R.defaultTo(null, db.escape(password))})
  `)
  .then(R.prop('insertId'))
  .then(getById(db))
)


const wherePassword = R.ifElse(
  R.isNil
, R.always('password IS NULL')
, (pw) => `password = '${pw}'`
)


const throwDuplicateRoom = R.curry((name, password) => {
  throw new Error(`
    Room (#{name}) with password (${password}) has duplicates
  `)
})


// doesExist = Mysql -> String -> String -> Room | Undefined
const doesExist = R.curry((db, name, password) =>
  getByNameAndPassword(db, name, password)
  .then(R.compose(R.lt(0), R.length))
)


module.exports = {
  getById
, deleteById
, getByNameAndPassword
, getOrCreateRoom
, insert
, doesExist
}
