
const R      = require('ramda')
const Domain = require('../lib/helper/domain.js')
const TABLE  = 'user'


const getAll = (db) => db.query(`SELECT * FROM ${TABLE}`)


const getById = Domain.getById(TABLE)


const insert = R.curry((db, username, room_id) =>
  db.query(`
    INSERT INTO ${TABLE}
    (room_id, name)
    VALUES('${room_id}', '${username}')
  `)
  .then(R.prop('insertId'))
  .then(getById(db))
)


// isInRoom :: Mysql -> String -> String -> Promise Bool
const isInRoom = R.curry((db, username, room_id) =>
  db.query(`
    SELECT * FROM ${TABLE}
    WHERE name = '${username}'
    AND room_id = '${room_id}'
    AND left_room = 0
    AND deleted = 0
  `)
  .then(R.compose(R.lt(0), R.length))
)


const getByRoomId = R.curry((db, room_id) =>
  db.query(`
    SELECT * FROM ${TABLE}
    WHERE room_id = ${db.escape(room_id)}
    AND left_room = 0
    AND deleted = 0
  `)
)

const leave = R.curry((db, id) =>
  db.query(`
    UPDATE \`${TABLE}\`
    SET \`left_room\` = 1
    WHERE \`id\` = ${db.escape(id)}
  `)
)


const deleteByRoomId = R.curry((db, room_id) =>
  db.query(`
    UPDATE ${TABLE}
    SET deleted = 1
    WHERE room_id = ${db.escape(room_id)}
  `)
)


module.exports = {
  getAll
, getById
, insert
, isInRoom
, getByRoomId
, leave
, deleteByRoomId
}
