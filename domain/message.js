
const R             = require('ramda')
const Awesomize     = require('../lib/awesomize.js')
const Domain        = require('../lib/helper/domain.js')
const TABLE         = 'message'
const USER          = 'user'
const MESSAGE_LIMIT = 501


const validate = Awesomize({}, (v) => ({
  room_id: { validate: [ v.required ] }
, user_id: { validate: [ v.required ] }
, message: { validate: [ v.required, v.isLength(1, MESSAGE_LIMIT) ]}
}))


const getById = R.curry((db, id) =>
  db.query(`
    SELECT M.*, U.name username FROM ${TABLE} M
    LEFT JOIN user U on U.id = M.user_id
    WHERE M.id = '${id}'
    AND M.deleted = 0
  `)
  .then(R.head)
)


const getByRoomId = R.curry((db, room_id) =>
  db.query(`
    SELECT M.*, U.name username FROM ${TABLE} M
    LEFT JOIN user U on U.id = M.user_id
    WHERE M.room_id = '${room_id}'
    AND M.deleted = 0
  `)
)


const insert = R.curry((db, {room_id, user_id, message}) =>
  db.query(`
    INSERT INTO ${TABLE}
    (room_id, user_id, message)
    VALUES('${room_id}', '${user_id}', ${db.escape(message)})
  `)
  .then(R.prop('insertId'))
  .then(getById(db))
)



const deleteByRoomId = R.curry((db, room_id) =>
  db.query(`
    UPDATE ${TABLE}
    SET deleted = 1
    WHERE room_id = ${db.escape(room_id)}
  `)
)


module.exports = {
  validate
, getById
, getByRoomId
, insert
, deleteByRoomId
}
