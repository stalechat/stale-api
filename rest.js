const mysql = require('mysql')

function REST_ROUTER(router,connection) {
  var self = this
  self.handleRoutes(router,connection)
}

REST_ROUTER.prototype.handleRoutes= function(router,connection) {

  router.get('/',function(req,res){
    res.sendFile(__dirname + '/index.html')
  })

}

module.exports = REST_ROUTER;
