
const R          = require('ramda')
const Bluebird   = require('bluebird')
const Middleware = require('./middleware.js')


const disconnectListener = (io, socket) =>

  socket.on('disconnect', () => console.log('a user disconnected'))


const joinRoomListener = (db, io, socket) =>

  socket.on('join', (data) => {

    console.log('A client connected to room: ', data)

    return Bluebird.props({
      join_socket: socket.join(data.room_id)
    , user : Middleware.getUserById(db, data.user_id)
    })
    .get('user')
    .then(user => io.in(data.room_id).emit('user_joined', user))

  })


const leaveRoomListener = (db, io, socket) =>

  socket.on('leave', (data) => {

    console.log('A client left room: ', data)

    return Middleware.leave(db, data)
    .then(() => socket.leave(data.room_id))
    .then(() => io.in(data.room_id).emit('user_left', { id: data.user_id }))

  })


const sendMessageListener = (db, io, socket) =>
  socket.on('send_message', (data) => {

    console.log('someone sent message : ', data)

    return Middleware.send_message(db, data)
    .then((msg) => io.in(data.room_id).emit('new_message', msg))

  })


const init = (db, io) => {

  io.on('connection', (socket) => {

    console.log('a user connected')

    joinRoomListener(db, io, socket)
    leaveRoomListener(db, io, socket)
    sendMessageListener(db, io, socket)
    disconnectListener(io, socket)

  })
}


module.exports = init
