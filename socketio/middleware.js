
const R        = require('ramda')
const Bluebird = require('bluebird')
const Message  = require('../domain/message.js')
const User     = require('../domain/user.js')
const Room     = require('../domain/room.js')


const _isRoomEmpty = R.curry((db, room_id) =>
  User.getByRoomId(db, room_id)
  .then(R.isEmpty)
)


const _deleteAllByRoomId = R.curry((db, room_id) =>
  Bluebird.all([
    Room.deleteById(db, room_id)
  , User.deleteByRoomId(db, room_id)
  , Message.deleteByRoomId(db, room_id)
  ])
)


const _deleteAllRoomIfEmpty = R.curry((db, room_id) =>
  _isRoomEmpty(db, room_id)
  .then(R.when(R.identity, () => _deleteAllByRoomId(db, room_id)))
)


const leave = R.curry((db, data) =>
  User.leave(db, data.user_id)
  .tap(() => _deleteAllRoomIfEmpty(db, data.room_id))
)


const send_message = R.curry((db, data) =>
  Bluebird.props({
    room_id : data.room_id
  , user_id : data.user_id
  , message : data.msg
  })
  .then(Message.validate)
  .then(Message.insert(db))
)


const getUserById = User.getById


module.exports = {
  leave
, send_message
, getUserById
}
