                     require('dotenv').config()
const Bluebird     = require('bluebird')
const R            = require('ramda')
const express      = require('express')
const mysql        = require('mysql')
const bodyParser   = require('body-parser')
const rest         = require('./rest.js')
const app          = express()
const http         = require('http').Server(app)
const io           = require('socket.io')(http)
const promisemysql = require('promise-mysql')
const config       = require('./config.json')
const SocketIo     = require('./socketio')


const PORT = (process.env.PORT || 5000)
const MYSQL_CONFIG = process.env.DEV ? process.env.MYSQL_URL : process.env.JAWSDB_URL


function REST(){
  var self = this
  self.connectPromiseMysql()
}


REST.prototype.connectPromiseMysql = function() {
  const self = this
  promisemysql.createConnection(MYSQL_CONFIG)
  .then(function(connection) {
    return self.configureSocketIo(connection)
  })

}


REST.prototype.configureSocketIo = function(pmysql) {
  const self = this
  SocketIo(pmysql, io)
  self.configureExpress(pmysql)
}


REST.prototype.configureExpress = function(pmysql) {
  var self = this
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())
  var router = express.Router()
  app.use('/', router)
  app.use('/api/user', require('./route/user.js')(pmysql))
  app.use('/api/room', require('./route/room.js')(pmysql))
  var rest_router = new rest(router,pmysql)
  self.startServer()
}


REST.prototype.startServer = function() {
  http.listen(PORT,function(){
      console.log(`Listening on 192.0.0.1:${PORT}.`)
  })
}


REST.prototype.stop = function(err) {
    console.log('ISSUE WITH MYSQL n' + err)
    process.exit(1)
}

new REST()
