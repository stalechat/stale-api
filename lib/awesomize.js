
const Awesomize                = require('awesomize')
const { throwValidationError } = require('./error.js')

module.exports = Awesomize.dataOrError(throwValidationError)
