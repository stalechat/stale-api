
const { curry } = require('ramda')


const createTable = curry((db, table, sql) => ({
  up   : (next) => db.query(sql, next)
, down : (next) => db.query(`DROP TABLE IF EXISTS \`${table}\``, next)
}))


module.exports = {
  createTable
}