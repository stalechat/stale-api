
const R = require('ramda')

const getById = (table) => R.curry((db, id) =>
  db.query(`
    SELECT * FROM ${table}
    WHERE id = '${id}'
    AND deleted = 0
  `)
  .then(R.head)
)

const deleteById = (table) => R.curry((db, id) =>
  db.query(`
    UPDATE ${table}
    SET deleted = 1
    WHERE id = '${id}'
  `)
)


module.exports = { getById, deleteById }
