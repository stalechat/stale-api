
const Bluebird     = require('bluebird')
const Yaldic       = require('yaldic')
const Mysql        = require('mysql')
const config       = require('../config.json')


module.exports = Yaldic({
  mysql : () => Mysql.createConnection(config.MYSQL)
})