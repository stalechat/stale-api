
const R         = require('ramda')
const Bluebird  = require('bluebird')
const Uuid      = require('uuid')
const Awesomize = require('../lib/awesomize.js')
const Err       = require('../lib/error.js')
const Room      = require('../domain/room.js')
const User      = require('../domain/user.js')
const Message   = require('../domain/message.js')


const _verifyUserUnique = R.curry((db, body) =>
  Room.getByNameAndPassword(db, body.room_name, body.password)
  .then(rows => {
    if (R.isEmpty(rows)) return
    return User.isInRoom(db, body.username, rows[0].id)
    .then(R.when(
      R.identity
    , () => Err.throwValidationError({username: 'not_unique'})))
  })
)


const validateJoin = Awesomize({}, (v) => ({
  room_name : {
    validate : [ v.required, v.isString, v.isLength(1,30) ]
  }
, password  : {
    validate : [ v.required, v.isString, v.isLength(1,30) ]
  }
, username : {
    validate : [ v.required, v.isString, v.isLength(1,30) ]
  }
}))


const join = (db) => (req, res) =>
  Bluebird.resolve(req.body)
  .then(validateJoin)
  .tap(_verifyUserUnique(db))
  .then(body =>
    Room.getOrCreateRoom(db, body.room_name, body.password)
    .then(room => Bluebird.props({
      room : room
    , user : User.insert(db, body.username, room.id)
    }))
  )
  .then(res.json.bind(res))
  .catch(res.json.bind(res))


const details = (db) => (req, res) =>
  Bluebird.props({ room_id : req.params.room_id })
  .then(Awesomize({}, (v) => ({
    room_id : { validate: [ v.required, v.isInt ] }
  })))
  .then(({room_id}) => Bluebird.props({
    room     : Room.getById(db)(room_id)
  , users    : User.getByRoomId(db)(room_id)
  , messages : Message.getByRoomId(db)(room_id)
  }))
  .then(res.json.bind(res))
  .catch(res.json.bind(res))


module.exports = {
  join
, details
}