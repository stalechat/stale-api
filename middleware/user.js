
const User = require('../domain/user.js')

const getAll = (db) => (req, res) => 
  User.getAll(db)
  .then(res.json.bind(res))


module.exports = {
  getAll
}