stale-api
========

api for stale chat

### Setup
1. Fork the main master branch

1. Clone your fork into your local machine

1. Move into the new *stale-api* directory

1. Globally install *nodemon* and *migration* (you may need sudo access)

    `npm install -g nodemon`

    `npm install -g migration`

1. Install npm packages

    `npm install`

1. Create a mysql database named *stale*

1. Run migrations

    `migrate up`

1. Start the server

    `npm start`