const Router  = require('express').Router()
const User    = require('../middleware/user.js')

const UserRouter = (mysql) => {

  Router.get('/', User.getAll(mysql))

  return Router
}


module.exports = UserRouter