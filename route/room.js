
const Bluebird = require('bluebird')
const Router   = require('express').Router()
const Room     = require('../middleware/room.js')


const RoomRouter = (mysql) => {

  Router.post('/', Room.join(mysql))

  Router.get('/:room_id', Room.details(mysql))

  return Router
}



module.exports = RoomRouter
