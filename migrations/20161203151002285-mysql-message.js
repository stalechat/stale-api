
const mysql           = require('../lib/container.js').get('mysql')
const { createTable } = require('../lib/helper/migration.js')

const TABLE = 'message'

const SQL   = `
  CREATE TABLE IF NOT EXISTS \`${TABLE}\` (
    \`id\` bigint(255) unsigned NOT NULL AUTO_INCREMENT,
    \`room_id\` bigint(255) unsigned NOT NULL,
    \`user_id\` bigint(255) unsigned NOT NULL,
    \`message\` text NOT NULL,
    \`created_timestamp\` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    \`updated_timestamp\` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    \`deleted\` tinyint(1) unsigned NOT NULL DEFAULT '0',
    PRIMARY KEY (\`id\`),
    KEY \`room_id\` (\`room_id\`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
`

module.exports = createTable(mysql, TABLE, SQL)
