
const mysql           = require('../lib/container.js').get('mysql')
const { createTable } = require('../lib/helper/migration.js')

const TABLE = 'user'

const SQL   = `
  CREATE TABLE IF NOT EXISTS \`user\` (
    \`id\` bigint(255) unsigned NOT NULL AUTO_INCREMENT,
    \`room_id\` bigint(255) unsigned NOT NULL,
    \`name\` varchar(30) NOT NULL DEFAULT '',
    \`left_room\` tinyint(1) NOT NULL DEFAULT '0',
    \`created_timestamp\` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    \`updated_timestamp\` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    \`deleted\` tinyint(1) unsigned NOT NULL DEFAULT '0',
    PRIMARY KEY (\`id\`),
    KEY \`room_id\` (\`room_id\`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
`

module.exports = createTable(mysql, TABLE, SQL)
