
const mysql           = require('../lib/container.js').get('mysql')
const { createTable } = require('../lib/helper/migration.js')

const TABLE = 'room'

const SQL   = `
  CREATE TABLE IF NOT EXISTS \`room\` (
    \`id\` int(11) unsigned NOT NULL AUTO_INCREMENT,
    \`name\` varchar(30) NOT NULL DEFAULT '',
    \`password\` varchar(30) DEFAULT NULL,
    \`created_timestamp\` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    \`updated_timestamp\` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    \`deleted\` tinyint(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (\`id\`),
    KEY \`name_password_compound\` (\`name\`,\`password\`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
`

module.exports = createTable(mysql, TABLE, SQL)
